classdef RulerImageCalibration < handle
  properties
    p = [1           1        1.7*570         1.2*570];
    %pp = [1426 1 1855 1001]; %size of dell monitor
    pp = [ 622         769        1855        1001]; %cave monitor
    fig; %main figure handle
    
    %Image Related
    flipY = true; %Reflect image in Y dirction
    
    %ROI
    xbounds;
    ybounds;
    DefaultROI = false;
    muX,muY; %x position for intergraton in pixels
    xwidth = 20; % pixels, integration bounds
    ywidth = 20;
    
    %Data
    imfile; %filename
    x,y; % n, m dim arrays
    R; %image matrix
    X0,Y0; %x,y cooridnates of graphical origin in pixels
    
    %Grid Data
    gridFile = 'RulerMicroscopeMeasurements.txt';
    selectOrigin =2;
    
    %FitResults
    g_pos,g_pos_y,g_pos_x; %aligned grid measurment array [mm]
    g_pos_err; % ydip fit error translated to grid position error [mm]
    ydips,xdips; %graphically identified dips at muX and muY
    Xpos,Xerr,Ypos,Yerr; %global fits of dips and associated fit errors
    yfits; %aligned dip fits array [px]
    yerr; %aligned dip fit err [px]
    c1;gof1;out1; %Linear Fit results
    sf_x,sf_y; %surface fit results
    
  end
  
  methods
     
    function [obj] = RulerImageCalibration(xbounds,ybounds)
      if nargin > 0
        obj.xbounds = xbounds;
        obj.ybounds = ybounds;
      else
        obj.DefaultROI = true;
      end
      obj.fig = figure('Position',obj.pp);
    end
    
    function[c1,out1,gof1]=CalibrateRulerImage(obj,filename,muX,muY)
      obj.PlotRulerWithOrigin(filename);
      [~,Iy] = obj.IntegrateImage(muX,muY);
      [ydip] = obj.IdentifyDips(Iy);
      obj.PlotDips(Iy,ydip)
      obj.FitDips(Iy,ydip);
      [c1,out1,gof1]=obj.FitToLine(obj.yfits, obj.yerr, obj.g_pos);
%       if nargin ==4
%           obj.SaveCalibration(savecalid)
%       end
    end
    
    function [R] = ReadImage(obj,filename)
      obj.imfile = filename;
      raw = imread(filename);
      R = double(raw(:,:,1));
      [m,~] = size(R);
      if obj.flipY
       for i = 1:m
           Rflip(m+1-i,:)= R(i,:); %flipping image
       end
       R = Rflip;
      end
      obj.R = R;  
    end
    
    function[X0,Y0,R,x,y] = PlotRulerWithOrigin(obj,filename)

      [R] = obj.ReadImage(filename);
      [m,n] = size(R);
      
      subplot(2,2,1:4)
      %Reshape for Fitting and Plotting
      x = 1:n;
      y = 1:m;
      [X,Y] = meshgrid(x,y);

      %%%%%%%%%%Zoom in/default bounds selection%%%%%%%%
      RF = 10; %Reduction factor for sampling matrix
      Rplot = R(1:RF:m,1:RF:n);
      Xplot = X(1:RF:m,1:RF:n);
      Yplot = Y(1:RF:m,1:RF:n);
      mesh(Xplot,Yplot,Rplot) 
      view(0,90)

      [xbounds2,ybounds2] = ginput(2);
      if obj.DefaultROI
        obj.xbounds = xbounds2;
        obj.ybounds = ybounds2;
      end

      %%%%%%%%%%%Origin Selection%%%%%%%%%%%%%%%%%%%%%%%%
      RF = 2; %Reduction factor for sampling matrix
      Rplot = R(1:RF:m,1:RF:n);
      Xplot = X(1:RF:m,1:RF:n);
      Yplot = Y(1:RF:m,1:RF:n);
      mesh(Xplot,Yplot,Rplot) 
      view(0,90)
      axis([xbounds2' ybounds2'])
      disp('Select visible origin with cursor click. Click twice.');
      k = waitforbuttonpress
      [X0,Y0] = ginput(1); %identify orgin dip

      subplot(2,2,1)
      mesh(X,Y,R)
      axis([obj.xbounds' obj.ybounds'])
      xlabel('X [px]')
      ylabel('Y [px]')
      title('Ruler Image')
      colorbar
      
      obj.x = x;
      obj.y = y;
      obj.Y0 = Y0;
      obj.X0 = X0;
    end

    function[I] = IntegrateImage(obj,mu, dir, width)
      %dir is direction over which to intergrate (i.e. 'X' means you get Iy
      %and 'Y' means you get Ix)
      if nargin <3
        dir = 'X';
      end
      
      if dir == 'X'
          coord = obj.x;
          obj.muX = mu;
          if nargin ==4
              obj.xwidth = width;
          else
              width = obj.xwidth;
          end
      elseif dir == 'Y'
          coord = obj.y;
          obj.muY = mu;
          if nargin ==4
              obj.ywidth = width;
          else
              width = obj.ywidth;
          end
      else
          disp('Error: 2nd argument dir should be direction to integrate over "X" or "Y"!');
          return;
      end
  

      bounds2 = [(mu-width),(mu+width)];
      
      ind(1) = find(coord>bounds2(1),1,'first');
      
      try
          ind(2) = find(coord>bounds2(2),1,'first');
      catch me
          ind(2) = length(coord);
      end
      if dir == 'X'
        I = sum(obj.R(:,ind(1):ind(2)),2); %integrate over xbounds
      else
        I = (sum(obj.R(ind(1):ind(2),:),1))'; %integrate over ybounds
      end
      
     
    end
    
    function[ydip,g_pos] = IdentifyDips(obj,Iy,dir) %%Graphically select intial guesses for dip centers. Return dip position selections and aligned grid positions from meausrment file.
      %identify dips in specified direction dir (i.e. "X" or "Y")
      if dir == 'X'
        coord = obj.x;
        C0 = obj.X0;
      else
        coord = obj.y;
        C0 = obj.Y0;
      end
      
      f1 = figure('Position',obj.pp);
      plot(coord,Iy)
      hold on
      plot([C0,C0],[min(Iy),max(Iy)],'r:','LineWidth',1.5)%plot line identifying origin
      %axis([700,1500,-inf,inf])
      drawnow
      pause(2)
      [ydip,~] = ginput; %collect points, press Return when done
      pause(2)
      close(f1);
      
      [~, ydip0_ind] = min(abs(ydip-C0)); %find index of nearest value to origin

      %Load Ruler Data
      [g_pos, g0_ind] = obj.LoadMeasurementFile();

      %Align arrays
      startIdx = (g0_ind-ydip0_ind+1);%Checks out
      endIdx = startIdx + length(ydip)-1;%Checks out
      endIdx = min(endIdx,length(g_pos));
      g_pos = g_pos(startIdx:endIdx) %Assuming length(ydip)<length(gpos) 
      ydip= ydip(1:endIdx-startIdx+1) %Resize to match g_pos if longer  
      
      obj.g_pos = g_pos;
      if dir == 'Y' 
        obj.ydips = ydip;
        obj.g_pos_y = g_pos;
      else
        obj.xdips = ydip; 
        obj.g_pos_x = g_pos;
      end
    end
    
    function [g_pos,g0_ind] = LoadMeasurementFile(obj,fname,selectOrigin)
      if nargin >1
        obj.gridFile = fname;
      end
      if nargin>2
        obj.selectOrigin = selectOrigin;
      end
      
      %Load Ruler Data
      RMeas = load(obj.gridFile);
      RMeasSubset = RMeas(find(~mod(RMeas(:,1),.5)),:); % grid line vertical positions, .5 mm increments, [mm]
      g_pos = RMeasSubset(:,2)'; % grid line vertical positions [mm] for half mm increments
      g0_ind = find(RMeasSubset(:,1)==obj.selectOrigin,1,'first'); %index of grid origin 
    end
    
    function []=PlotDips(obj,Iy,ydip,dir)
        if dir == 'Y'
          xibounds = [obj.muX-obj.xwidth,obj.muX+obj.xwidth]; 

          %figure(obj.fig)
          %subplot(2,2,3);
          plot(obj.y,Iy)
          xlabel('Y [px]')
          title({'X Integrated Image';['[' num2str(xibounds,'%.0f %.0f') '] [px]']})

          hold on
          plot([obj.Y0,obj.Y0],[min(Iy),max(Iy)],'r:','LineWidth',1.5)%plot lineidentifying origin
          plot([obj.ybounds(1),obj.ybounds(1)],[min(Iy),max(Iy)],'g','LineWidth',1.5)
          plot([obj.ybounds(2),obj.ybounds(2)],[min(Iy),max(Iy)],'g','LineWidth',1.5)
      %     patch([ybounds(1) ybounds(2) ybounds(2) ybounds(1)]',[min(Iy),min(Iy),max(Iy),max(Iy)]','g')

          ymin = min([ydip(1),obj.Y0,obj.ybounds(1)]);
          ymax = max([ydip(end),obj.Y0,obj.ybounds(2)]);
        else
          xibounds = [obj.muY-obj.ywidth,obj.muY+obj.ywidth]; 

          %figure(obj.fig)
          %subplot(2,2,3)
          plot(obj.x,Iy)
          xlabel('X [px]')
          title(['Y Integrated Image [' num2str(xibounds) '] [px]'])

          hold on
          plot([obj.X0,obj.X0],[min(Iy),max(Iy)],'r:','LineWidth',1.5)%plot lineidentifying origin
          plot([obj.xbounds(1),obj.xbounds(1)],[min(Iy),max(Iy)],'g','LineWidth',1.5)
          plot([obj.xbounds(2),obj.xbounds(2)],[min(Iy),max(Iy)],'g','LineWidth',1.5)
      %     patch([ybounds(1) ybounds(2) ybounds(2) ybounds(1)]',[min(Iy),min(Iy),max(Iy),max(Iy)]','g')

          ymin = min([ydip(1),obj.X0,obj.xbounds(1)]);
          ymax = max([ydip(end),obj.X0,obj.xbounds(2)]);
        end
        
        axis([ymin-10,ymax+10,100,inf])
    end
    
    function[yfits,yerr]=FitDips(obj,y,Iy,ydip)
        %figure(obj.fig) %assume plot in current axes
        %subplot(2,2,3)
      
      %Select fit bounds/range for each lorenzian
      f = .3;
      fit_lo = ydip - f*mean(diff(ydip));
      fit_hi = ydip + f*mean(diff(ydip));

      i0 = find(y>ydip(1),1);
      i1 = find(y>ydip(2),1);
      m = 0; %m = (Iy(i1)-Iy(i0))/(ydip(end)-ydip(1));
      yflat = ydip + (mean(diff(ydip))/2);%b = 5500;
      [~, i3] = histc(yflat,y);
      
      b = mean(Iy(nonzeros(i3)))
      A= 4000; %A = 2000;


      %fitOptions.Exclude = outliers;
      for i=1:length(ydip)
          funString = 'p7 + Lorentzian(x,p1,p2,p3)+Lorentzian(x,p4,p5,p6)';
          fitType = fittype(funString,...
              'independent',{'x'}, 'dependent', {'y'},...
              'coefficients',{'p1','p2','p3','p4','p5','p6','p7'});
          fitOptions = fitoptions(fitType); %custom
          fitOptions.StartPoint = [A,ydip(i)-f,f,A,ydip(i)+f,f,b];
          fitOptions.Upper = [0,ydip(i),inf,0,fit_hi(i),inf,inf];
          fitOptions.Lower = [-4*A,fit_lo(i),-inf,-4*A,ydip(i),-inf,0];
          fitOptions.Exclude = excludedata(y',Iy,'Domain',[fit_lo(i),fit_hi(i)]);

          [cfun,out,gof] = fit(y',Iy,fitType,fitOptions)
          xplot = linspace(fit_lo(i),fit_hi(i),100);
          plot(xplot,feval(cfun,xplot),'r')

          yfits(i) = (cfun.p2 + cfun.p5)/2; %midpoint between peaks
          CF = confint(cfun,.68);
          yerr(i) = sqrt((CF(2,2)-cfun.p2)^2 +(CF(2,5)-cfun.p5)^2);

      end
      obj.yfits = yfits;
      obj.yerr = yerr;
    end
    
    function[Xpos,Xerr,Ypos,Yerr] = FitAllDips(obj,xpos,ypos)
      m = length(ypos);
      n = length(xpos);
      Xpos = zeros(m,n);
      Xerr = zeros(m,n);
      Ypos = zeros(m,n);
      Yerr = zeros(m,n);        
      
      hold on
      ax = gca;
      

      %scan accross X first
      fx = figure('Position',obj.pp);
      subplot(1,n,1)         
      for j = 1:n
        Iy = obj.IntegrateImage(xpos(j),'X');
        subplot(1,n,j)
        
        obj.PlotDips(Iy,ypos,'Y');
        [yfits,yerr] = obj.FitDips(obj.y,Iy,ypos);
        view(90,90)
        Ypos(:,j) = yfits;
        Yerr(:,j) = yerr;
        j
      end
      
      %scan across Y
      fy = figure('Position',obj.pp);
      subplot(m,1,1) 
      for i = 1:m
        Ix = obj.IntegrateImage(ypos(i),'Y');
        subplot(m,1,m-i+1)
        obj.PlotDips(Ix,xpos,'X');
        [xfits,xerr] = obj.FitDips(obj.x,Ix,xpos);
        Xpos(i,:) = xfits;
        Xerr(i,:) = xerr;
        
      end
      
      obj.Xpos = Xpos;
      obj.Ypos = Ypos;
      obj.Xerr = Xerr;
      obj.Yerr = Yerr;
     
    end
    
    function[] = PlotRulerDipPositions(obj,Xpos,Ypos)
      figure(obj.fig)
      subplot(2,2,1)
      hold on
      Zmax = max(max(obj.R));
      [m,n] = size(Xpos);
      Zmark = Zmax*ones(m,n);
      h = plot3(Xpos,Ypos,Zmark,'k+','MarkerSize',15,'Linewidth',2);
    end
    
    function[sf_x,sf_y] = FitToSurface(obj,Xpx,Ypx,Xpx_err,Ypx_err,Xmm,Ymm)
      %lower index is always lower value in both i and j to y and x
      
      x_px = reshape(Xpx,1,[])';
      y_px = reshape(Ypx,1,[])';
      x_px_err = reshape(Xpx_err,1,[])';
      y_px_err = reshape(Ypx_err,1,[])';
      
      
      x_mm = reshape(Xmm,1,[])';
      y_mm = reshape(Ymm,1,[])';
      
      weights_x = 1;
      weights_y = 1;
      fitname = 'poly11';
      sf_x = fit([x_px,y_px],x_mm,fitname,'Weights', weights_x*ones(length(x_px),1));
      x_mm_err = sqrt((sf_x.p10*x_px_err).^2 + (sf_x.p01*y_px_err).^2);
      weights_x = (1./x_mm_err).^2;
      ok = ~isnan(weights_x);
      [sf_x,out_x] = fit([x_px(ok),y_px(ok)],x_mm(ok),fitname,'Weights', weights_x(ok));
      
      sf_y = fit([x_px,y_px],y_mm,fitname,'Weights', weights_y*ones(length(y_px),1));
      y_mm_err = sqrt((sf_y.p10*x_px_err).^2 + (sf_y.p01*y_px_err).^2);
      weights_y = (1./y_mm_err).^2;
      ok = ~isnan(weights_y);
      [sf_y,out_y] = fit([x_px(ok),y_px(ok)],y_mm(ok),fitname,'Weights', weights_y(ok));
      
      figure(obj.fig)
      
      subplot(2,2,2)
      xfit = sf_x(x_px,y_px);
      Xfit = reshape(xfit,size(Xmm));
      residuals_x = x_mm-xfit;
      res_X = Xmm-Xfit;
      %scatter3(x_px, y_px,residuals_x,'filled')
      ok_x = x_mm_err<5;
      hold off
      plot3d_errorbars(x_px(ok_x), y_px(ok_x), residuals_x(ok_x), x_mm_err(ok_x))
      hold on
      %res_X(~ok_x)=nan;
      mesh(Xpx,Ypx,res_X)
      xlabel('X grid position [px]')
      ylabel('Y grid position [px]')
      zlabel('xIdeal - xFit [mm]')
      title('X Residuals')
      view(45,45)
      
      ylims=get(gca,'ylim');
      xlims=get(gca,'xlim');
      xrange=xlims(2)-xlims(1);
      yrange=ylims(2)-ylims(1);
      %for northwest corner
      %adjust the 0.8 or 0.9 as needed
      xpos=xlims(1)+0.1*xrange;
      ypos=ylims(1)+0.73*yrange;
      DisplayFitStats('other',sf_x,out_x.rmse^2,xpos,ypos)
      
      subplot(2,2,4)
      yfit = sf_y(x_px,y_px);
      Yfit = reshape(yfit,size(Ymm));
      residuals_y = y_mm-yfit;
      res_Y = Ymm-Yfit;
      %scatter3(x_px, y_px,residuals_y,'filled')
      ok_y = y_mm_err<5;
      hold off
      plot3d_errorbars(x_px(ok_y), y_px(ok_y), residuals_y(ok_y), y_mm_err(ok_y))
      hold on
      mesh(Xpx,Ypx,res_Y)
      xlabel('X grid position [px]')
      ylabel('Y grid position [px]')
      zlabel('yIdeal - yFit [mm]')
      title('Y Residuals')
      view(45,45)
      
      ylims=get(gca,'ylim');
      xlims=get(gca,'xlim');
      xrange=xlims(2)-xlims(1);
      yrange=ylims(2)-ylims(1);
      %for northwest corner
      %adjust the 0.8 or 0.9 as needed
      xpos=xlims(1)+0.1*xrange;
      ypos=ylims(1)+0.73*yrange;
      
      DisplayFitStats('other',sf_y,out_y.rmse^2,xpos,ypos)
      
      subplot(2,2,3)
      hold off
      scale = 50;
      ok_both = ok_x&ok_y;
      hq = quiver(x_mm(ok_both),y_mm(ok_both),scale*residuals_x(ok_both),scale*residuals_y(ok_both),0);
      xlabel('X [mm]')
      ylabel('Y [mm]') 
      title(['Deviations from Ideal Position x' num2str(scale)])
    
      obj.sf_x = sf_x;
      obj.sf_y = sf_y;
    end
    
    function[Xmm, Ymm] = PlotCalibratedImage(obj,R)
             [m,n] = size(R);
      figure('Position',obj.pp);
      %Reshape for Fitting and Plotting
      x = 1:n;
      y = 1:m; 
      [X,Y] = meshgrid(x,y);

      Xmm = reshape(obj.sf_x(X(:),Y(:)),size(X));
      Ymm = reshape(obj.sf_y(X(:),Y(:)),size(Y));


      
      mesh(Xmm,Ymm,R);
      xlabel('X [mm]')
      ylabel('Y [mm]')
      title('Calibrated Image')
      view(0,90)
      colorbar
    end
    
    function[c1,out1,gof1]=FitToLine(obj,ydip,yerr,g_pos)
      %Fit
      weights = 1;%/.4^2;%rms uncertainty estimate on g_pos (effective uncertainty determined grid line locations in pixels)
      [c1,out1,gof1] = fit(ydip',g_pos','poly1','Weights',weights*ones(length(ydip),1));
      g_pos_err = c1.p1*yerr;
      weights = (1./g_pos_err).^2;
      [c1,out1,gof1] = fit(ydip',g_pos','poly1','Weights',weights);
      
      
      figure(obj.fig)
      subplot(2,2,2)
      errorbar(ydip',g_pos,g_pos_err,'r*')     
      hold on
      plot(c1,'b')
      xlabel('Y grid position [px]')
      ylabel('Y grid position [mm]')
      title('Linear Fit (pixels vs mm)')
      ylims=get(gca,'ylim');
      xlims=get(gca,'xlim');
      xrange=xlims(2)-xlims(1);
      yrange=ylims(2)-ylims(1);

      %for northwest corner
      %adjust the 0.8 or 0.9 as needed
      xpos=xlims(1)+0.1*xrange;
      ypos=ylims(1)+0.73*yrange;
      DisplayFitStats('other',c1,out1.rmse^2,xpos,ypos)
      legend('data','fit')

      % %check chi2
      % yfit = c1(ydip);
      % chi2nu = sum(weights.*(yfit-ydip).^2)/(out1.dfe)

      subplot(2,2,4)
      yfit = c1(ydip');
      residuals = g_pos-yfit';
      bar(ydip',residuals,'b')
      xlabel('Y grid position [px]')
      ylabel('yMeas - yFit [mm]')
      title('Residuals')
      
      obj.g_pos_err = g_pos_err;
      obj.c1 = c1;
      obj.out1 = out1;
      obj.gof1 = gof1;

      SetFigureProperties(2,16,16)

    end
  
%     function[] = SaveCalibration(obj,savecalid)
%        save(['RulerCal_' num2str(savecalid,'%04d')],'obj.c1','obj.out1','obj.gof1','obj.filename','obj.muX','obj.yfits','obj.yerr','obj.g_pos')
%     end
  end
end
      


