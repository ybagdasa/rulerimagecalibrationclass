function [] = DisplayFitStats(fitType,cf1_,chisqr1,xpos,ypos,label)
conf1 = confint(cf1_,.68); %95% confidence intervals by default
names = coeffnames(cf1_);
values = coeffvalues(cf1_);
form = formula(cf1_);
n = length(names);

if nargin ==3 || isempty(xpos)
      ylims=get(gca,'ylim');
      xlims=get(gca,'xlim');
      xrange=xlims(2)-xlims(1);
      yrange=ylims(2)-ylims(1);

      %for northwest corner
      %adjust the 0.8 or 0.9 as needed
      xpos=xlims(1)+0.1*xrange;
      ypos=ylims(1)+0.73*yrange;
end

if nargin <6
    label = '';
end
    
if(strcmp(fitType,'poly2'))
    text(xpos,ypos,{
        form;
        strcat('p1 = ',num2str(cf1_.p1),' \pm ',num2str(abs(conf1(1,1)-cf1_.p1)));
        strcat('p2 = ',num2str(cf1_.p2,'%0.5f'),' \pm ',num2str(abs(conf1(1,2)-cf1_.p2),'%0.5f'));
        strcat('p3 = ',num2str(cf1_.p3,'%0.2f'),' \pm ',num2str(abs(conf1(1,3)-cf1_.p3),'%0.2f'));
        strcat('\chi^2_\nu =',num2str(chisqr1))
        },'FontSize',14);%,strcat('Monitor Fit (m = ',num2str(cf2_.p1),', b = ',num2str(cf2_.p2),', \chi^2_\nu =',num2str(chisqr2),')'))
else if (strcmp(fitType,'poly1'))
    text(xpos,ypos,{
        form;
        strcat('a = ',num2str(cf1_.p1),' (',num2str(conf1(1,1)),',',num2str(conf1(2,1)),')');
        strcat('b = ',num2str(cf1_.p2),' (',num2str(conf1(1,2)),',',num2str(conf1(2,2)),')');
        strcat('\chi^2_\nu =',num2str(chisqr1))
        },'FontSize',14);%,strcat('Monitor Fit (m = ',num2str(cf2_.p1),', b = ',num2str(cf2_.p2),', \chi^2_\nu =',num2str(chisqr2),')'))
else
    statstr = cell(n+3,1);

    for i = 1:n
        statstr(i+2,1) = strcat(names(i),' = ',num2str(values(i),'%0.2f'), ' \pm ', num2str(abs(conf1(1,i)-values(i)),'%0.2f'));
    end
    statstr(1,1) = cellstr(label);
    statstr(2,1) = cellstr(form);
    statstr(n+3,1) = cellstr(strcat('\chi^2_\nu = ', num2str(chisqr1,'%0.2f')));
    text(xpos,ypos,statstr,'FontSize',14);
end

end