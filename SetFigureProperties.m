function [] = SetFigureProperties(LineWidth,AxisSize,TitleFontSize,LegFontSize,TextSize,MarkerSize)
%% Enter as last line of figure code block to format line, axis, and title properties.
%Written by Yelena Bagdasarova 2011-2018

if nargin < 5, TextSize = 15; end
if nargin < 4, LegFontSize = 15; end
if nargin < 3, TitleFontSize = 15; end
if nargin < 2, AxisSize = 15; end
if nargin < 1, LineWidth = 1; end
if nargin < 6, MarkerSize = 2*LineWidth; end
set(findall(gcf,'type','text'),'FontSize',TextSize) %set all text to size 18 by default

hLeg = findobj(gcf,'Type','axes','Tag','legend'); %all legend objects of figure
hAllAxes = findobj(gcf,'Type','axes'); %all axes objects of figure (including legend objects)
hAxes = setdiff(hAllAxes,hLeg); % all axes objects of figure excluding legend objects

ht = get(hAxes,'title');
hx = get(hAxes,'xlabel');
hy = get(hAxes,'ylabel');
hz = get(hAxes,'zlabel');
hl = get(hAxes,'Children');

hc = get(gcf,'Children');
hLeg = findobj(hc,'Tag','legend');

set(hAxes,'FontSize',AxisSize); %axes tick font 
set(hLeg,'FontSize',LegFontSize) %legend text font

if length(hAxes)>1
    
    set([ht{:}],'FontSize',TitleFontSize);
    set([hx{:}],'FontSize',AxisSize);
    set([hy{:}],'FontSize',AxisSize);
    set([hz{:}],'FontSize',AxisSize);
    [m,n] = size(hl); % m is 
    for i=1:m
        try
            set([hl{i}],'LineWidth',LineWidth);
            set([hl{i}],'MarkerSize',MarkerSize);
        catch ME
            %warning(ME.message)
        end
    end
else
    set(ht,'FontSize',TitleFontSize);
    set(hx,'FontSize',AxisSize);
    set(hy,'FontSize',AxisSize);
    set(hz,'FontSize',AxisSize);
    set(hl,'LineWidth',LineWidth);
    try    
        set(hl,'MarkerSize',MarkerSize);
    catch ME
    end
end
end