close all
clear
clc

filename = 'Center.TIF';


muX=270;
muY=300;
textfile = 'TestPoints.txt'; 

mycal =RulerImageCalibration;
mycal.PlotRulerWithOrigin(filename);
mycal.xwidth = 50;
mycal.ywidth = 50;
[Iy] = mycal.IntegrateImage(muX,'X');
[Ix] = mycal.IntegrateImage(muY,'Y');
mycal.gridFile = textfile;
mycal.selectOrigin = 0;

 [ydip] = mycal.IdentifyDips(Iy,'Y');
 [xdip] = mycal.IdentifyDips(Ix,'X');
 figure(mycal.fig)
 subplot(2,2,3);
 mycal.PlotDips(Iy,ydip,'Y')
 mycal.PlotDips(Ix,xdip,'X')
 [yfits,yerr] = mycal.FitDips(mycal.y,Iy,ydip);
 [xfits,xerr] = mycal.FitDips(mycal.x,Ix,xdip);
 [Xpos,Xerr,Ypos,Yerr] = mycal.FitAllDips(xfits,yfits);

 mycal.PlotRulerDipPositions(Xpos,Ypos);
%  [m,n] =size(Xpos);
%  xmm = -7:.25:(n-1)*.25-7;
%  ymm = -7:.25:(m-1)*.25-7;
xmm = mycal.g_pos_x;
ymm = mycal.g_pos_y;
 [Xmm,Ymm] = meshgrid(xmm,ymm);
 [sf_x,sf_y] = mycal.FitToSurface(Xpos,Ypos,Xerr,Yerr,Xmm,Ymm);
 
 mycal.PlotCalibratedImage(mycal.R);

 

%  [c1,out1,gof1]=mycal.FitToLine(mycal.yfits, mycal.yerr, mycal.g_pos);