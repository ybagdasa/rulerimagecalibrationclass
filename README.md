The functions in the class file **RulerImageCalibration.m** are used in the script **CalTest.m** to calibrate an image of a ruler grid (**Center.TIF**) taken by a camera through a focussing lens. The obtained polynomial function can then be used to map the pixel coordinates of a given camera image to mm coordinates relative to an origin, correcting any distortions introduced by the lens. 

![](https://bitbucket.org/ybagdasa/rulerimagecalibrationclass/raw/master/output_1100.gif)

To do this, the pixel positions of the grid lines along each axis (X and Y) are first graphically identified by the user (via point and click). The grid lines show up as a series of dips in integrated sections of the image in X and Y, as shown in blue in two figures below. The user-identified positions are used as initial parameters for an algorithm that fits each dip to a pair of Lorentizian curves (fits shown in red). The dip locations in *$X_{px}$* and *$Y_{px}$* are then fit against measured or ideal grid crossing locations in mm (**TestPoints.txt**) with polynomials *$X_{mm}(X_{px},Y_{px})$* and *$Y_{mm}(X_{px},Y_{px})$*. 

![](https://bitbucket.org/ybagdasa/rulerimagecalibrationclass/raw/master/XDipFits_CenterTest_resize.png)

![](https://bitbucket.org/ybagdasa/rulerimagecalibrationclass/raw/master/YDipFits_CenterTest_resize.png)

The figure below shows the identified grid crossings on the ruler image (top-left), the surface fit residuals in X and Y (top and bottom right), and an arrow plot of the residuals at each location (bottom-left).

![](https://bitbucket.org/ybagdasa/rulerimagecalibrationclass/raw/master/SurfFits_CenterTest_resize.png)